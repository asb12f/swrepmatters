---
title: '#SWRepMatters Engagement January 2018'
output:
  html_document: default
---

```{r Login, message=FALSE, warning=FALSE, include=FALSE}
# Let's load some packages
library(rtweet)
library(tm)
library(SnowballC)
library(wordcloud)
library(RColorBrewer)
library(ggplot2)

source("login.R") # this will open a window to confirm app authorization
```

![SWRepMatters](cover.png "Star Wars Representation Matters")

It's time for another round of #SWRepMatters analysis! If you missed last time, you can find the stats here:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">In October, I ran an analysis of the <a href="https://twitter.com/hashtag/SWRepMatters?src=hash&amp;ref_src=twsrc%5Etfw">#SWRepMatters</a> campaign. It was impressive - 2230 unique tweets, 870 users, and a whole lot of love. <br><br>This month, we did it again. It was a bit smaller, but still worthwhile. Check out the results here: <a href="https://t.co/W4Ifwu7pjY">https://t.co/W4Ifwu7pjY</a> <a href="https://t.co/cmF8kJCUZU">pic.twitter.com/cmF8kJCUZU</a></p>&mdash; Andrew (@andrew_benesh) <a href="https://twitter.com/andrew_benesh/status/932757345058410496?ref_src=twsrc%5Etfw">November 20, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


##  Getting the Tweets

First, let's pull the most recent 10,000 tweets using the #SWRepMatters hashtag for the past 6-9 days (current Twitter API indexing does not allow for longer searches).  
```{r, message=FALSE, warning=FALSE, include=FALSE}
SWRep <- search_tweets("#SWRepMatters", n=10000,  retryonratelimit=TRUE, include_rts = FALSE)
```
```{r, message=FALSE, warning=FALSE, include=FALSE}
SWRep2 <- search_tweets("#SWRepMatters", n=10000,  retryonratelimit=TRUE, include_rts = TRUE)
```

We were able to collect tweets from `r min(SWRep2$created_at)` to `r max(SWRep2$created_at)` using the hashtag #SWRepMatters.
It looks like there were `r NROW(SWRep)` unique tweets using the hashtag during the designated time period (`r NROW(SWRep2)` counting retweets). 

## Who Tweeted?
```{r, include=FALSE}
WhoDidIt <- sort(table(SWRep$screen_name), decreasing = TRUE)
WhoDidIt2 <- sort(table(SWRep2$screen_name), decreasing = TRUE)
```
A total of `r NROW(WhoDidIt)` users contributed tweets, and `r NROW(WhoDidIt2)` users shared them in retweets. 

### The top 20 contributors were:

```{r, echo=FALSE}
WhoDidIt[1:20]
```

### The top 20 sharers were:

```{r, echo=FALSE}
WhoDidIt2[1:20]
```

## What items were most retweeted?
```{r, message=FALSE, warning=FALSE, include=FALSE}
maxItem <- which.max(SWRep$retweet_count)
url <- paste("https://twitter.com/",SWRep$screen_name[maxItem][[1]], "/status/", SWRep$status_id[maxItem], sep="")

```

The most widely retweeted tweet was by @`r SWRep$screen_name[maxItem][[1]]`, and can be found at `r url`. 



```{r, message=FALSE, warning=FALSE, include=FALSE}
maxItem <- which.max(SWRep$favorite_count)
url <- paste("https://twitter.com/",SWRep$screen_name[maxItem][[1]], "/status", SWRep$status_id[maxItem], sep="")
```
The most widely favorited tweet was by @`r SWRep$screen_name[maxItem][[1]]`, and can be found at `r url`. 

## How many people were tweeting and when? In 30 minute intervals:
```{r, echo=FALSE, message=FALSE, warning=FALSE}
#p <- ts_plot(SWRep, by="1800 secs", 
#        theme = "apa", 
#        xtime = "%D %H:%S", 
#        cex.main = 1,
#        main = "Tweets per 30mins", 
#        subtitle = "#SWRepMatters",
#        adj=FALSE)

filtered <- ts_data(SWRep,
  "1800 secs"
)

p <- filtered %>%
  ggplot(aes(x = time, y = n)) + 
  geom_line() +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  labs(title = "#SWRepMatters Tweets",
       x = "Time",
       y = "Tweets / 30 Mins")

p

```

## What did people tweet about?
```{r, echo=FALSE, message=FALSE, warning=FALSE}
SWRep$text <- enc2utf8(SWRep$text)
SWRep$text <- sapply(SWRep$text,function(row) iconv(row, "latin1", "ASCII", sub=""))
text <- paste(unlist(SWRep$text))

docs <- Corpus(VectorSource(text))
#inspect(docs)
toSpace <- content_transformer(function (x , pattern ) gsub(pattern, " ", x))
docs <- tm_map(docs, toSpace, "/")
docs <- tm_map(docs, toSpace, "@")
docs <- tm_map(docs, toSpace, "\\|")
# Convert the text to lower case
docs <- tm_map(docs, content_transformer(tolower))

# Remove numbers
docs <- tm_map(docs, removeNumbers)
# Remove english common stopwords
docs <- tm_map(docs, removeWords, stopwords("english"))


# specify your stopwords as a character vector
docs <- tm_map(docs, removeWords, c("swrepmatters", "tco ", "star","wars","and", "for", "TCO", "https")) 
# Remove punctuations
docs <- tm_map(docs, removePunctuation)
# Eliminate extra white spaces
docs <- tm_map(docs, stripWhitespace)
# Text stemming
#docs <- tm_map(docs, stemDocument)
# Remove your own stop word
docs <- tm_map(docs, removeWords, c("swrepmatters", "tco ", "star","wars","and", "for", "TCO", "https")) 

#Building a term Document Matrix
dtm <- TermDocumentMatrix(docs)
m <- as.matrix(dtm)
v <- sort(rowSums(m),decreasing=TRUE)
d <- data.frame(word = names(v),freq=v)

#Make A Cloud
set.seed(1234)
p <- wordcloud(words = d$word, freq = d$freq, min.freq = 5,
          max.words=500, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))
```

## Where are these people?
```{r, echo=FALSE, message=FALSE, warning=FALSE}
#where <- sort(table(SWRep$place_full_name), decreasing = TRUE)
#plot(where)

locations <- lat_lng(SWRep)

#with(locations, plot(lng, lat))

worldMap <- map_data("world")  # Easiest way to grab a world map shapefile

zp1 <- ggplot(worldMap)
zp1 <- zp1 + geom_path(aes(x = long, y = lat, group = group),  # Draw map
                       colour = gray(2/3), lwd = 1/3)
zp1 <- zp1 + geom_point(data = locations,  # Add points indicating users
                        aes(x = lng, y = lat),
                        colour = "RED", alpha = 1/2, size = 1)
zp1 <- zp1 + coord_equal()  # Better projections are left for a future post
zp1 <- zp1 + theme_minimal()  # Drop background annotations
zp1
```


## Conclusion

This time around we had a slightly larger level of engagement - more tweets, more retweets, more original tweeters, and a wider group of retweeters. We didn't have as geographically diverse of a sample and we didn't rival the first #SWRepMatters campaign, but we still made an important point! People still want to see themselves on screen, hear their own voices in text, and experience their own perspectives across media. #SWRepMatters.


